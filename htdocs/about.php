<!DOCTYPE html>
<html>
<head>
  <meta charset="UTF-8">
  <title>CS3140 assignment 02 for Justin Balser</title>
  <link rel="stylesheet" href="styles.css">
</head>

<body>

    <header>
        <a href="#my_account">My Account</a>
        <a href="#wish_list">Wish List</a>
        <a href="#shopping_cart">Shopping Cart</a>
        <a href="#checkout">Checkout</a>
    </header>

      <h1 style="text-align:left">Art Store</h1>

  <ul>
    <li><a href="index.html">Home</a></li>
    <li><a href="about.html">About Us</a></li>
    <li><a href="#art_works">Art Works</a></li>
    <li><a href="#artists">Artists</a></li>
  </ul>

  <br>



<div style="margin: 0px 250px">
  <h2>About Us</h2>
    <p>
      This assignment was created by Justin Balser.
      <br>
      <br>
      It was created for CS3140 at Bowling Green State University
    </p>
</div>

<br>

<footer>
All images are copyright to their owners. This is just a hypothetical site © 2014 Copyright Art Store
</footer>

</body>
</html>
