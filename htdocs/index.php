<!DOCTYPE html>
<html>
<head>
  <meta charset="UTF-8">
  <title>CS3140 assignment 02 for Justin Balser</title>
  <link rel="stylesheet" href="styles.css">
</head>

<body>

    <header>
        <a href="#my_account">My Account</a>
        <a href="#wish_list">Wish List</a>
        <a href="#shopping_cart">Shopping Cart</a>
        <a href="#checkout">Checkout</a>
    </header>



      <h1 style="text-align:left";
      font = "Helvetica Neue">Art Store</h1>

  <ul>
    <li><a href="index.html">Home</a></li>
    <li><a href="about.html">About Us</a></li>
    <li><a href="#art_works">Art Works</a></li>
    <li><a href="#artists">Artists</a></li>
  </ul>
  <br>


<div style="margin: 0px 250px">
  <h2>Self-Portrait in Straw Hat</h2>
    <p>
      By <a href="https://www.nationalgallery.org.uk/paintings/international-womens-day-elisabeth-louise-vigee-le-brun", style="color:blue"; style="text-decoration:underline"><u>Louise Elisabeth Lebrun</u></a>
    </p>
    <img src="113010.jpg" alt="photo" width ="250px" style="float:left;margin:0px 20px" align="left">
    The painting appears, after cleaning, to be an autograph replica of a picture, the original<br> of which was painted in Brussels in 1782 in free imitation of Ruebens's 'Chapeau de Paille',<br> which LeBrun had seen in Antwerp. It was exhibited in Paris in 1782 at the Salon de la<br> Correspondence.

<br>
<br>
<p style="color:red;"font:"Arial";>$700</p>


  <button class="button">Add to Wish List</button>
        <button class="button">Add to Shopping Cart</button>

<h3>Product Details</h3>

<table>
  <tr>
    <td><b>Date:</b></td>
    <td>1782</td>
  </tr>
  <tr>
    <td><b>Medium:</b></td>
    <td>Oil on canvas</td>
  </tr>
  <tr>
    <td><b>Dimensions:</b></td>
    <td>98cm x 71cm</td>
  </tr>
  <tr>
    <td><b>Home:</b></td>
    <td><a href="https://www.nationalgallery.org.uk/", style="color:blue"; style="text-decoration:underline"><u>National Gallery, London</u></a></td>
  </tr>
  <tr>
    <td><b>Genres:</b></td>
    <td><a href="https://en.wikipedia.org/wiki/Realism_(arts)", style="color:blue"; style="text-decoration:underline"><u>Realism</u></a>, <a href="https://en.wikipedia.org/wiki/Rococo", style="color:blue"; style="text-decoration:underline"><u>Rococo</u></a></td>
  </tr>
  <tr>
    <td><b>Subjects:</b></td>
    <td><a href="https://en.wikipedia.org/wiki/People", style="color:blue"; style="text-decoration:underline"><u>People</u></a>, <a href="https://en.wikipedia.org/wiki/Art", style="color:blue"; style="text-decoration:underline"><u>Arts</u></a></td>
  </tr>
</table>


<p>LeBrun's original is recorded in a private <br>collection in France.</p>
<h2>Similar Products</h2>
  <div class="row">
    <div class="column">
      <img src="116010.jpg" alt="Thistle" width = "150px">
      <br>
      <a href="https://en.wikipedia.org/wiki/Portrait_of_the_Artist_Holding_a_Thistle", style="color:blue"; style="text-decoration:underline"><u>Artist Holding a Thistle</u></a>
    </div>
    <div class="column">
      <img src="120010.jpg" alt="Eleanor" width = "150px">
      <br>
      <a href="https://en.wikipedia.org/wiki/Portrait_of_Eleanor_of_Toledo", style="color:blue"; style="text-decoration:underline"><u>Portrait of Eleanor of Toledo</u></a>
    </div>
    <div class="column">
      <img src="107010.jpg" alt="Pompadour" width = "150px">
      <br>
      <a href="https://en.wikipedia.org/wiki/Madame_de_Pompadour", style="color:blue"; style="text-decoration:underline"><u>Madame de Pompadour</u></a>
    </div>
    <div class="column">
      <img src="106020.jpg" alt="Pearl" width = "150px">
      <br>
      <a href="https://en.wikipedia.org/wiki/Girl_with_a_Pearl_Earring", style="color:blue"; style="text-decoration:underline"><u>Girl with a Pearl Earring</u></a>
    </div>
  </div>
</div>

<footer>
All images are copyright to their owners. This is just a hypothetical site © 2014 Copyright Art Store
</footer>

</body>
</html>
